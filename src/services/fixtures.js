export default class Fixtures {

  users = [
    {
      firstName: 'Homer',
      lastName: 'Simpson',
      email: 'homer@simpson.com',
      password: 'secret'
    },
    {
      firstName: 'Marge',
      lastName: 'Simpson',
      email: 'marge@simpson.com',
      password: 'secret'
    },
    {
      firstName: 'Bart',
      lastName: 'Simpson',
      email: 'bart@simpson.com',
      password: 'secret'
    }
  ];

  tweets = [
    {
      date: '2016-11-04T19:03:39.000Z',
      text: 'Doh!',
      user: this.users[0]
    },
    {
      date: '2016-11-04T19:03:39.000Z',
      text: 'Why I outta...',
      user: this.users[0]
    },
    {
      date: '2016-11-04T19:03:39.000Z',
      text: 'Kids!!',
      user: this.users[1]
    },
    {
      date: '2016-11-04T19:03:39.000Z',
      text: 'Homer my man..',
      user: this.users[2]
    },
    {
      date: '2016-11-04T19:03:39.000Z',
      text: 'Sigh...',
      user: this.users[1]
    }
  ];
}
