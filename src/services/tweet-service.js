import {inject} from 'aurelia-framework';
import Fixtures from './fixtures';

@inject(Fixtures)
export default class TweetService {

  users = []
  tweets = [];

  constructor(data) {
    this.users = data.users;
    this.tweets = data.tweets;
  }

  addTweet(date, tweetText) {
    const tweet = {
      date: date,
      text: tweetText
      //user: user
    };
    this.tweets.push(tweet);
    console.log('User tweeted: \"' + tweetText + '\" @: ' + date);
  }

  register(firstName, lastName, email, password) {
    const newUser = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password
    };
    this.users[email] = newUser;
  }

  login(email, password) {
    const status = {
      success: false,
      message: ''
    };

    this.users.forEach(user => {
      if ((email === user.email) && (password === user.password)) {
        console.log(`Logging in ${this.email}`);
        status.success = true;
        status.message = 'logged in';
      } else {
        console.log(`Failed to login with ${this.email}`);
        //status.message = 'Failed to login with credentials';
      }
    });

    return status;
  }
}
